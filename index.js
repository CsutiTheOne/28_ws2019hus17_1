var instructions = document.getElementById("instructions");
var letterSize = 15;
var bigText = 40;
var planeSize = [200,100];
var starSize = 60;
var birdSize = [100,70];
var displaySize = [
	[100, 90],
	[10, 100, 53],
	[100, 100, 250]
];
var buttonSize = 24;
var GameField = document.getElementById("GameField");

var soundON;
var GameStarted;
var pause;
var gameOver = false;

var plane = document.getElementById("plane").style;
var parachute = document.getElementById("parachute").style;
var star = document.getElementById("star").style;

var background = new Audio("background.mp3");
var hit = new Audio("hit.mp3");
var starSound = new Audio("star.mp3")
var finish = new Audio("finish.mp3");

function load() {
	soundON = true;
	pause = false
	GameStarted = false;
	instructions.style.fontSize = letterSize + "px";
	document.getElementById("startButton").style.fontSize = letterSize + 5 + "px";
	plane.width = planeSize[0] + "px";
	plane.height = planeSize[1] + "px";
	star.width = starSize + "px";
	star.height = starSize + "px";
	parachute.width = starSize + "px";
	parachute.height = starSize + "px";
	document.getElementById("starCounter").style.fontSize = "letterSize" + 5 + "px";
	document.getElementById("timer").style.fontSize = letterSize + "px";
	
}

function sizePlus() {
	if (letterSize<=18){
		letterSize++;
		bigText+=2;
		planeSize[0] += 10;
		planeSize[1] += 5;
		starSize += 5;
		birdSize[0] += 10;
		birdSize[1] += 7;
		displaySize[0][0] += 10;
		displaySize[0][1] += 9;
		displaySize[1][0] += 1;
		displaySize[1][1] += 10;
		displaySize[1][2] += 2;
		displaySize[2][0] += 10;
		displaySize[2][1] += 10;
		displaySize[2][2] -= 10;
		buttonSize += 2;
		instructions.style.fontSize = letterSize + "px";
		document.getElementById("startButton").style.fontSize = letterSize + 5 + "px";
		//incrase plane size
		plane.width = planeSize[0] + "px";
		plane.height = planeSize[1] + "px";
		//decrase star size
		star.width = starSize + "px";
		star.height = starSize + "px";
		//incrase parachute size
		parachute.width = starSize + "px";
		parachute.height = starSize + "px";
		//incrase bird size
		bird.style.width = birdSize[0] + "px";
		bird.style.height = birdSize[1] + "px";
		//incrase fuel and star counter size
		document.getElementById("fuelBar").style.width = displaySize[0][0]  + "px";
		document.getElementById("fuelBar").style.height = displaySize[0][1] + "px";
		document.getElementById("fuelCounter").style.width = displaySize[1][0] + "px";
		document.getElementById("fuelCounter").style.height = displaySize[1][1] + "px";
		document.getElementById("fuelCounter").style.left = displaySize[1][2] + "px";
		document.getElementById("starIcon").style.width = displaySize[2][0] + "px";
		document.getElementById("starIcon").style.height = displaySize[2][1] + "px";
		document.getElementById("starIcon").style.left = displaySize[2][2] + "px";
		//other text size
		document.getElementById("PauseText").style.fontSize = bigText + "px";
		document.getElementById("EndText").style.fontSize = bigText + "px";
		document.getElementById("retry").style.fontSize = letterSize + "px";
		document.getElementById("starCounter").style.fontSize = letterSize + 5 + "px";
		document.getElementById("timer").style.fontSize = letterSize + "px";
		document.getElementsByClassName("smallbutton").style.width = buttonSize + "px";
		document.getElementsByClassName("smallbutton").style.height = buttonSize + "px";
		
		
	}
}
function sizeMinus() {
	if (letterSize>=8) {
		letterSize--;
		bigText -= 2;
		planeSize[0] -= 10;
		planeSize[1] -= 5;
		starSize -= 10;
		birdSize[0] -= 10;
		birdSize[1] -= 7;
		displaySize[0][0] -= 10;
		displaySize[0][1] -= 9;
		displaySize[1][0] -= 1;
		displaySize[1][1] -= 10;
		displaySize[1][2] -= 2;
		displaySize[2][0] -= 10;
		displaySize[2][1] -= 10;
		displaySize[2][2] += 10;
		buttonSize -= 2;
		instructions.style.fontSize = letterSize + "px";
		document.getElementById("startButton").style.fontSize = letterSize + 5 + "px";
		//decrase plane size
		plane.width = planeSize[0] + "px";
		plane.height = planeSize[1] + "px";
		//decrase star size
		star.width = starSize + "px";
		star.height = starSize + "px";
		//decrase parachute size
		parachute.width = starSize + "px";
		parachute.height = starSize + "px";
		//decrase bird size
		bird.style.width = birdSize[0] + "px";
		bird.style.height = birdSize[1] + "px";
		//decrase fuel and star counter size
		document.getElementById("fuelBar").style.width = displaySize[0][0]  + "px";
		document.getElementById("fuelBar").style.height = displaySize[0][1] + "px";
		document.getElementById("fuelCounter").style.width = displaySize[1][0] + "px";
		document.getElementById("fuelCounter").style.height = displaySize[1][1] + "px";
		document.getElementById("starIcon").style.left = displaySize[1][2] + "px";
		document.getElementById("starIcon").style.width = displaySize[2][0] + "px";
		document.getElementById("starIcon").style.height = displaySize[2][1] + "px";
		document.getElementById("starIcon").style.left = displaySize[2][2] + "px";
		//other text size
		document.getElementById("PauseText").style.fontSize = bigText + "px";
		document.getElementById("EndText").style.fontSize = bigText + "px";
		document.getElementById("retry").style.fontSize = letterSize + "px";
		document.getElementById("starCounter").style.fontSize = "letterSize" + 5 + "px";
		document.getElementById("timer").style.fontSize = letterSize + "px";
		document.getElementsByClassName("smallbutton").style.width = buttonSize + "px";
		document.getElementsByClassName("smallbutton").style.height = buttonSize + "px";

		
	}
}
function SoundOnOff() {
		if (soundON==true){
			soundON = false;
			document.getElementById("SoundOnOff").src = "28_img/SoundOFF.png";
			background.pause();
			hit.pause();
			starSound.pause();
			finish.pause();

		}
		else {
			soundON = true;
			document.getElementById("SoundOnOff").src = "28_img/SoundON.png";
			if(gameOver==false && GameStarted == true && pause==false){
				background.play();
			}
		}
}
function pauseGame() {
	if (GameStarted == true){
		if(gameOver==false){
			if (pause == false ) {
				pause = true;
				document.getElementById("PauseText").style.display = "block";
				background.pause();
				hit.pause();
				starSound.pause();
			}
			else {
				pause = false;
				document.getElementById("PauseText").style.display = "none";
				if(soundON==true){
					background.play();
				}
			}
		}
		else if (gameOver==true){
				startGame();
		}
	}
}


var y = 300;
var x = 100;
function move() {
	switch (event.keyCode) {
		case 37:	//left
			if (pause==false && gameOver==false){
				if(x>0){
					x-=20;
					plane.left = x + "px";
				}
			}
			break;
		case 38:	//up
			if (pause==false && gameOver==false){
				if(y<640){
					y+=20;
					plane.bottom = y + "px";	
				}
			}
			break;
		case 39:	//right
			if (pause==false && gameOver==false){
				if (x<780){
					x+=20;
					plane.left = x + "px";
				}
			}
			break;
		case 40:	//down
			if (pause==false && gameOver==false){
				if(y>0){
					y-=20;
					plane.bottom = y + "px";
				}
			}
			break;
		case 32:
			pauseGame();
			break;
	}
}
function startGame() {
	gameOver = false;
	GameStarted = true;
	document.getElementById("startButton").style.display = "none";//hide the "start game" button
	instructions.style.display = "none"; //hide instructions
	document.getElementById("timer").style.display = "block"; //make timer visible
	document.getElementById("fuelCounter").style.display = "block";
	document.getElementById("fuelBar").style.display = "block";
	document.getElementById("starCounter").style.display = "block";
	document.getElementById("starIcon").style.display = "block";
	document.getElementById("EndText").style.display = "none";
	if(soundON==true){background.play()}
	

	//start position of plane
	y = 300;
	x = 100;
	plane.display = "block"; //make plane show up
	plane.bottom = y + "px";
	plane.left = x + "px";
	//make plane movable
	// IE9, Chrome, Safari, Opera
	window.addEventListener("keydown", move());
	// Firefox
	window.addEventListener("DOMKeydown", move());


	//everything about the clouds
	var clouds = {
		cloud1: {
			obj: document.getElementById("cloud1").style, 
			x: Math.floor(Math.random() *224), 
		},
		cloud2: {
			obj: document.getElementById("cloud2").style,
			x: Math.floor(Math.random() *224) + 224,
		},
		cloud3: {
			obj: document.getElementById("cloud3").style,
			x: Math.floor(Math.random() *376) + 448,
		}
	};
	
	//first spawn of clouds
	clouds["cloud1"]["obj"].right = clouds["cloud1"]["x"] + "px";	//first cloud
	clouds["cloud1"]["obj"].bottom = Math.floor(Math.random() *668) + "px";
	clouds["cloud1"]["obj"].display = "block";
	clouds["cloud2"]["obj"].right = clouds["cloud2"]["x"] + "px";	//secund cloud
	clouds["cloud2"]["obj"].bottom = Math.floor(Math.random() *668) + "px";
	clouds["cloud2"]["obj"].display = "block";
	clouds["cloud3"]["obj"].right = clouds["cloud3"]["x"] + "px";	//third cloud
	clouds["cloud3"]["obj"].bottom = Math.floor(Math.random() *668) + "px";
	clouds["cloud3"]["obj"].display = "block";

	//first bird spawn
	var bird = document.getElementById("bird");
	var brdX = 924;
	var birdFrame = 1;
	bird.style.display="block";
	bird.style.left = brdX + "px";
	bird.style.bottom =  Math.floor(Math.random()*688)+10 + "px";
	//first parachute spawn
	var fuelOn = true;
	var parachuteSize = 1;
	var prchtY = 700;;
	parachute.display = "block";
	parachute.bottom = prchtY + "px";
	parachute.left = Math.floor(Math.random() *964) + 10 + "px";
	//first star spawn
	var starOn = true;
	var starFrame = 1;
	var strY = 700;
	star.display = "block";
	star.bottom = strY + "px";
	star.left = Math.floor(Math.random() *964) + 10 + "px";


	//for timer
	var runTime = 0;
	var Sec = 0;
	var Min = 0;
	//for Fuel counter
	var Fuel = 10;
	var degree = -24.0;
	//for star counter
	var starCount = 0;
	document.getElementById("starCounter").innerHTML = starCount;

	var interval = setInterval(loop, 100);
	function loop(){
		if(gameOver==false){
			if (pause==false){
				//replace the cloud if reach left border
				if(clouds["cloud1"]["x"]>=824){
					clouds["cloud1"]["x"] = 0;
					clouds["cloud1"]["obj"].right = clouds["cloud1"]["x"] + "px";
					clouds["cloud1"]["obj"].bottom = Math.floor(Math.random()*668) + "px";
				}
				if(clouds["cloud2"]["x"]>=824){
					clouds["cloud2"]["x"] = 0;
					clouds["cloud2"]["obj"].right = clouds["cloud2"]["x"] + "px";
					clouds["cloud2"]["obj"].bottom = Math.floor(Math.random()*668) + "px";
				}
				if(clouds["cloud3"]["x"]>=824){
					clouds["cloud3"]["x"] = 0;
					clouds["cloud3"]["obj"].right = clouds["cloud3"]["x"] + "px";
					clouds["cloud3"]["obj"].bottom = Math.floor(Math.random()*668) + "px";
				}
				//move clouds
				clouds["cloud1"]["x"] += 5;
				clouds["cloud1"]["obj"].right = clouds["cloud1"]["x"] + "px";
				clouds["cloud2"]["x"] += 5;
				clouds["cloud2"]["obj"].right = clouds["cloud2"]["x"] + "px";
				clouds["cloud3"]["x"] += 5;
				clouds["cloud3"]["obj"].right = clouds["cloud3"]["x"] + "px";
				
				//replace bird when reach left border
				if (brdX<=20){
					brdX = 890;
					bird.style.left = brdX + "px";
					bird.style.bottom = Math.floor(Math.random()*688)+10 + "px";
				}
				//Move bird
				else if(brdX>5){
				brdX -= 25;
				bird.style.left = brdX + "px";
				}
				//animate the bird
				switch(birdFrame){
					case 1:
						birdFrame++;
						bird.src = "28_img/frame-2.png";
					break;
					case 2:
						birdFrame++;
						bird.src = "28_img/frame-3.png";
					break;
					case 3:
						birdFrame++;
						bird.src = "28_img/frame-4.png";
					break;
					case 4:
						birdFrame=1;
						bird.src = "28_img/frame-1.png";
					break;
				}
				//if parachute reach left side
				if (prchtY<=70){
					fuelOn = true;
					parachute.display = "block";
					prchtY = 700;
					parachute.bottom = prchtY + "px";
					parachute.left = Math.floor(Math.random() *708) + "px";
				}
				//move parachute
				else if(prchtY >= 60){
					prchtY -= 10;
					parachute.bottom = prchtY + "px";
				}
				//animate parachute
				if(parachuteSize==1){
					parachuteSize++
					parachute.width = starSize + 10 + "px";
					parachute.height = starSize + 10 + "px";
				}
				else if (parachuteSize==5){
					parachuteSize++
					parachute.width = starSize + "px";
					parachute.height = starSize + "px";
				}
				else if (parachuteSize==10){
					parachuteSize=1;
				}
				else {
					parachuteSize++
				}
				//if star reach the bottom
				if (strY<=70){
					starOn = true;
					star.display = "block";
					strY = 700;
					star.bottom = strY + "px";
					star.left = Math.floor(Math.random() *964) + 10 + "px";
				}
				//move star
				else if (strY>70){
					strY -= 20;
					star.bottom = strY + "px";
				}
				//animate star
				switch(starFrame){
					case 1:
						starFrame++;
						document.getElementById("star").src = "28_img/star2.png";
					break;
					case 2:
						starFrame++;
						document.getElementById("star").src = "28_img/star3.png";
					break;
					case 3:
						starFrame=1;
						document.getElementById("star").src = "28_img/star1.png";
					break;
				}

				//timer, every 10th times the loop() runs counts 1 seccund
				runTime+=1
				if(runTime==10){
					runTime=0;
					Sec++;
					Fuel--;
					degree-=2.1
					document.getElementById("fuelCounter").style.transform = "rotate(" + degree + "deg)";
				}
				if(Sec>=59){
					Sec=0;
					Min++;
				}
				document.getElementById("timer").innerHTML = Min + ":" + Sec;
				if(Fuel<=0){
					Over();
				}
				//The coordinates of the center of the plane
				var centerX = x + 200 / 2;
				var centerY = y + 100 / 2;
				//center coordinates of bird
				var brdCenterX = brdX + 100 / 2;
				var brdCenterY = parseInt(bird.style.bottom) + 70 / 2;

				//Here comes the check of the distance between 2 elements
				function DistanceCheck(x2,y2){
					var a = centerX - x2;
					var b = centerY - y2;
					var c = Math.sqrt( a*a + b*b );
					return(c);
				}

				//distance parameters
				var dBird = DistanceCheck(brdCenterX,brdCenterY); //distance between bird and plane
				var dParachute = DistanceCheck(parseInt(parachute.left), prchtY); // distance between parachute and plane
				var dStar = DistanceCheck(parseInt(star.left) ,strY);  //distance berween a star and the plane
				//If plane too close to a bird
				if(dBird<60){
					if(soundON==true){
						hit.play();
					}
					Over();
				}
				//if plane meet a parachute
				if (dParachute <= 100){
					if(fuelOn == true){
						if(Fuel<=31){
							Fuel+=10;
							degree += 24.0;
							document.getElementById("fuelCounter").style.transform = "rotate(" + degree + "deg)";
							parachute.display = "none"
							fuelOn = false;
						}
					}
				}
				//if plane wants to collect a star
				if (dStar <= 90){
					if(starOn == true){
						starOn = false;
						star.display = "none";
						starCount++;
						document.getElementById("starCounter").innerHTML = starCount;
						star.display = "none";
						if(soundON==true){
							starSound.play();
						}
					}
				}
				function Over(){
					gameOver = true;
					document.getElementById("EndText").style.display = "block";
					clearInterval(interval);
					if(soundON==true){
						background.pause();
						starSound.pause();
						finish.play();
					}
				}
				if(background.ended == true){
					if(soundON == true){
						background.play();
					}
				}
			}
		}
	}
}